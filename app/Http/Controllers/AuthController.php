<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthController extends Controller
{
    //
    public function register(Request $request)
    {
        try {
            $validatedData = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                // 'email' => 'required|string|email|max:255|unique:users',
                'username' => 'required|unique:users',
                'phone' => 'required|unique:users',
                'password' => 'required|string|min:8',
            ]);

            if ($validatedData->fails()) {
                return response()->json([
                    'status' => false,
                    'message'  => $validatedData->errors()->first()
                ]);
            }

            $user = User::create([
                'name' => $request['name'],
                'email' => isset($request['email']) ? $request['email'] : $request['email'],
                'username' => $request['username'],
                'phone' => $request['phone'],
                'password' => Hash::make($request['password']),
            ]);

            $token = $user->createToken('auth_token')->plainTextToken;
            $user['access_token'] = $token;
            $user->update();

            return response()->json([
                'status' => true,
                'message' => 'Register successfully.',
                'data' => [
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                ]
            ]);
        } catch (\Exception $err) {
            return response()->json([
                'status' => false,
                'message' => $err->getMessage()
            ]);
        }
    }

    public function login(Request $request)
    {
        $user = array(
            'username' => $request->username,
            'password' => $request->password,
        );
        if (Auth::attempt($user)) {
            return response()->json([
                    'status' => true,
                    // 'message' => 'Welcome back '.$user->name,
                    'message' => "Login successfully",
                    'data' => [
                        'user' => Auth::user(),
                    ]
                ]);
        } else {
            return response()->json(['message' => 'Fail']);
        }
    }

    public function me(Request $request)
    {
       try {
        //code...
        if(Auth::user()){
            return response()->json([
                'status' => true,
                'message' => '',
                'data' => [
                'user' => $request->user(),
                ]
            ]);
        }else{
            return response()->json("user khong ton tai ");
        }
       } catch (\Throwable $th) {
        //throw $th;
       }  
    }
    public function index(Request $request)
    {
        try{
            if(Auth::user() != null){
                $phantrang = User::orderBy('id','DESC')->where('phanquyen','0')->paginate(4);
                return response()->json($phantrang);
            }
        } catch (\Throwable $th) {
            // throw $th;
            return response()->json(['message'=>$th]);
        }
    }

    public function update(Request $request)
    {
        $user = User::findOrFail($request->input('id'));
        if($request->input('password')){
            $request['password'] = Hash::make($request->input('password'));
        }
        $user->update($request->all());
        return response()->json([
            'status' => true,
            'data' => [
                        'user' => $user,
                      ],
            'message' => 'Update info successfully! Please login again!',
        ]);
    }
    public function update2(Request $request)
    {
        $user = User::where('id',$request->id);
        $user->update([
            'phanquyen' => "1",
        ]);
        return response()->json("ok");
    }
}
