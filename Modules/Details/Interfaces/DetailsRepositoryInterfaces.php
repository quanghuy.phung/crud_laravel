<?php

namespace Modules\Product\Interfaces;


interface DetailsRepositoryInterfaces {
    public function index();
    public function store();
}