-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2023 at 08:32 AM
-- Server version: 10.4.25-MariaDB
-- PHP Version: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_04_27_133255_create_stores_table', 2),
(6, '2022_05_01_031731_create_menus_table', 3),
(7, '2022_05_07_031359_create_orders_table', 4),
(8, '2022_05_15_041249_create_qtyfoods_table', 5),
(9, '2022_05_16_142015_create_bookings_table', 6),
(10, '2022_05_23_232926_create_foodsupplies_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(6, 'App\\Models\\User', 26),
(7, 'App\\Models\\User', 13),
(7, 'App\\Models\\User', 32),
(7, 'App\\Models\\User', 33),
(7, 'App\\Models\\User', 34),
(8, 'App\\Models\\User', 34),
(9, 'App\\Models\\User', 40),
(9, 'App\\Models\\User', 44),
(11, 'App\\Models\\User', 7),
(11, 'App\\Models\\User', 13),
(11, 'App\\Models\\User', 26);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type_id` smallint(6) NOT NULL,
  `user_order_id` int(11) DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `money` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status_order_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_detail` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL,
  `user_owner_id` int(11) DEFAULT NULL,
  `payment_id` smallint(6) DEFAULT NULL,
  `payment_status` tinyint(1) DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `momo_id` int(22) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `type_id`, `user_order_id`, `phone`, `address`, `money`, `status_order_id`, `name`, `order_detail`, `store_id`, `user_owner_id`, `payment_id`, `payment_status`, `notes`, `momo_id`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, NULL, NULL, '45000', 0, NULL, '[{\"id\":7,\"price\":45000,\"qty\":1,\"pic\":\"\\/1652372759.png\",\"name\":\"G\\u1ecfi chay xanh\"}]', 1, 2, 0, 1, NULL, NULL, '2022-05-18 09:25:00', '2022-05-18 09:38:51'),
(3, 0, NULL, NULL, NULL, '45000', 0, NULL, '[{\"id\":7,\"price\":45000,\"qty\":1,\"pic\":\"\\/1652372759.png\",\"name\":\"G\\u1ecfi chay xanh\"}]', 1, 2, 0, 1, NULL, NULL, '2022-05-18 10:13:22', '2022-05-18 10:20:02'),
(4, 1, 4, '0360000003', '01 Trần Não Q2', '45000', 4, 'Nguyễn Thanh Khang', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":1}]', 1, NULL, 1, 1, NULL, NULL, '2022-05-18 10:23:54', '2022-05-18 10:41:30'),
(5, 1, 4, '0360000003', '123', '90000', 4, 'Nguyễn Thanh Khang', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":2,\"isChoose\":true,\"oldQty\":2}]', 1, 2, 1, 1, NULL, NULL, '2022-05-18 10:42:55', '2022-05-18 10:45:28'),
(7, 1, 4, '0360000003', '123', '90000', 4, 'Nguyễn Thanh Khang', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":2,\"isChoose\":true,\"oldQty\":2}]', 1, 2, 1, 1, NULL, NULL, '2022-05-18 10:48:19', '2022-05-18 11:32:55'),
(8, 1, 4, '0360000003', '123', '45000', 4, 'Nguyễn Thanh Khang', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":1,\"isChoose\":true,\"oldQty\":1}]', 1, 2, 1, 1, NULL, NULL, '2022-05-18 11:15:51', '2022-05-18 11:45:14'),
(9, 0, NULL, NULL, NULL, '85000', 0, NULL, '[{\"id\":6,\"price\":85000,\"qty\":1,\"pic\":\"\\/1651382988.png\",\"name\":\"C\\u01a1m tr\\u1ed9n Chay Xanh\"}]', 2, 5, 0, 1, NULL, NULL, '2022-05-18 11:27:40', '2022-05-18 11:27:51'),
(11, 1, 6, '012345678', '123', '45000', 0, 'thien1221', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":1}]', 1, NULL, 1, 0, NULL, NULL, '2022-05-20 19:52:16', '2022-05-20 19:52:16'),
(25, 1, 4, '0360000003', '123', '85000', 1, 'Nguyễn Thanh Khang', '[{\"name\":\"C\\u01a1m tr\\u1ed9n Chay Xanh\",\"pic\":\"\\/1651382988.png\",\"id\":6,\"price\":85000,\"qty\":1}]', 1, NULL, 3, 1, NULL, 1653157428, '2022-05-21 11:23:48', '2022-05-21 11:41:24'),
(26, 1, 4, '0360000003', '123', '45000', 1, 'Nguyễn Thanh Khang', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":1}]', 1, NULL, 3, 1, NULL, 1653158560, '2022-05-21 11:42:40', '2022-05-21 11:43:02'),
(27, 1, 4, '0360000003', '123', '45000', 1, 'Nguyễn Thanh Khang', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":1}]', 1, NULL, 3, 1, NULL, 1653158652, '2022-05-21 11:44:12', '2022-05-21 11:44:31'),
(35, 1, 4, '0360000003', '123', '45000', 1, 'Nguyễn Thanh Khang', '[{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":1,\"isChoose\":true,\"oldQty\":1}]', 1, 3, 3, 1, NULL, 1653193893, '2022-05-21 21:31:33', '2022-05-22 07:59:54'),
(37, 1, 4, '0360000003', '4 Lê Lợi', '45000', 4, 'Nguyễn Thanh Khang', '[{\"id\":7,\"price\":45000,\"qty\":1,\"pic\":\"\\/1652372759.png\",\"name\":\"G\\u1ecfi chay xanh\",\"isChoose\":true,\"oldQty\":1}]', 1, 8, 1, 1, NULL, 1653227265, '2022-05-22 13:47:45', '2022-05-22 16:00:32'),
(38, 0, NULL, NULL, NULL, '45000', 0, NULL, '[{\"id\":7,\"price\":45000,\"qty\":1,\"pic\":\"\\/1652372759.png\",\"name\":\"G\\u1ecfi chay xanh\"}]', 1, 8, 0, 0, NULL, NULL, '2022-05-22 14:28:50', '2022-05-22 14:28:50'),
(39, 0, NULL, NULL, NULL, '85000', 0, NULL, '[{\"id\":6,\"price\":85000,\"qty\":1,\"pic\":\"\\/1651382988.png\",\"name\":\"C\\u01a1m tr\\u1ed9n Chay Xanh\"}]', 1, 8, 0, 0, NULL, NULL, '2022-05-22 14:31:14', '2022-05-22 14:31:14'),
(41, 0, NULL, NULL, NULL, '80000', 4, NULL, '[{\"id\":7,\"price\":45000,\"qty\":1,\"pic\":\"\\/1652372759.png\",\"name\":\"G\\u1ecfi chay xanh\",\"isChoose\":true,\"oldQty\":1},{\"id\":12,\"price\":35000,\"qty\":1,\"pic\":\"\\/1652928127.png\",\"name\":\"Canh rong bi\\u1ec3n\",\"isChoose\":true,\"oldQty\":1}]', 1, 8, 0, 1, NULL, NULL, '2022-05-22 14:53:21', '2022-05-22 16:57:19'),
(42, 1, 8, '0110000001', '123', '465000', 0, 'quanlyquan2', '[{\"name\":\"Canh rong bi\\u1ec3n\",\"pic\":\"\\/1652928127.png\",\"id\":12,\"price\":35000,\"qty\":1},{\"name\":\"C\\u1ee7 sen chi\\u00ean l\\u1eafc ph\\u00f4 mai\",\"pic\":\"\\/1652927080.png\",\"id\":8,\"price\":35000,\"qty\":1},{\"name\":\"G\\u1ecfi chay xanh\",\"pic\":\"\\/1652372759.png\",\"id\":7,\"price\":45000,\"qty\":1},{\"name\":\"Ch\\u1ea3 gi\\u00f2 chay xanh\",\"pic\":\"\\/1652927236.png\",\"id\":9,\"price\":40000,\"qty\":1},{\"name\":\"C\\u01a1m chi\\u00ean Chay Xanh\",\"pic\":\"\\/1651382957.png\",\"id\":5,\"price\":80000,\"qty\":1},{\"name\":\"C\\u01a1m tr\\u1ed9n Chay Xanh\",\"pic\":\"\\/1651382988.png\",\"id\":6,\"price\":85000,\"qty\":1},{\"name\":\"S\\u00fap n\\u1ea5m m\\u1ed1i\",\"pic\":\"\\/1652927711.png\",\"id\":11,\"price\":65000,\"qty\":1},{\"name\":\"N\\u1ea5m kho h\\u1ea1t sen\",\"pic\":\"\\/1652927537.png\",\"id\":10,\"price\":80000,\"qty\":1}]', 1, NULL, 1, 0, NULL, 1653234773, '2022-05-22 15:52:53', '2022-05-22 15:52:53'),
(44, 1, 4, '0360000003', '4 Lê Lợi', '35000', 4, 'Nguyễn Thanh Khang', '[{\"name\":\"Canh rong bi\\u1ec3n\",\"pic\":\"\\/1652928127.png\",\"id\":12,\"price\":35000,\"qty\":1,\"isChoose\":true,\"oldQty\":1}]', 1, 8, 3, 1, NULL, 1653235975, '2022-05-22 16:12:55', '2022-05-22 16:34:26'),
(46, 1, 4, '0360000003', '4 Lê Lợi', '105000', 4, 'Nguyễn Thanh Khang', '[{\"name\":\"Canh rong bi\\u1ec3n\",\"pic\":\"\\/1652928127.png\",\"id\":12,\"price\":35000,\"qty\":2,\"isChoose\":true,\"oldQty\":2},{\"id\":8,\"price\":35000,\"qty\":1,\"pic\":\"\\/1652927080.png\",\"name\":\"C\\u1ee7 sen chi\\u00ean l\\u1eafc ph\\u00f4 mai\",\"isChoose\":true,\"oldQty\":1}]', 1, 8, 1, 1, '123', 1653240930, '2022-05-22 17:35:30', '2022-05-22 18:55:37'),
(60, 0, NULL, NULL, NULL, '140000', 4, NULL, '[{\"id\":12,\"price\":35000,\"qty\":1,\"pic\":\"\\/1652928127.png\",\"name\":\"Canh rong bi\\u1ec3n\"},{\"id\":8,\"price\":35000,\"qty\":3,\"pic\":\"\\/1652927080.png\",\"name\":\"C\\u1ee7 sen chi\\u00ean l\\u1eafc ph\\u00f4 mai\"}]', 1, 3, 0, 1, 'qwe', NULL, '2022-05-24 19:04:35', '2022-05-24 19:04:46'),
(61, 1, 4, '0360000003', '4 Lê Lợi', '35000', 0, 'Nguyễn Thanh Khang', '[{\"name\":\"Canh rong bi\\u1ec3n\",\"pic\":\"\\/1652928127.png\",\"id\":12,\"price\":35000,\"qty\":1}]', 1, NULL, 3, 1, NULL, 1653421028, '2022-05-24 19:37:08', '2022-05-24 19:37:35');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'edit articles', 'api', '2022-12-19 20:50:20', '2022-12-19 20:50:20'),
(2, 'delete articles', 'api', '2022-12-19 20:51:46', '2022-12-19 20:51:46'),
(3, 'add articles', 'api', '2022-12-19 20:52:00', '2022-12-19 20:52:00'),
(4, 'add genre', 'api', '2022-12-19 20:55:48', '2022-12-19 20:55:48'),
(5, 'delete genre', 'api', '2022-12-19 20:56:24', '2022-12-19 20:56:24'),
(6, 'dev_view', 'api', '2022-12-19 20:56:24', '2022-12-19 20:56:24'),
(7, 'prod_view', 'api', '2022-12-19 20:56:24', '2022-12-19 20:56:24'),
(8, 'write', 'api', '2022-12-19 20:56:24', '2022-12-19 20:56:24'),
(9, 'edit', 'api', '2022-12-19 20:56:24', '2022-12-19 20:56:24'),
(10, 'admin', 'api', '2022-12-19 20:56:24', '2022-12-19 20:56:24');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 1, 'auth_token', '4e15e2163c13c901eb13e5ae9972fb0b01c0b91618c7fc412c4d25861f394941', '[\"*\"]', '2022-05-24 19:05:21', '2022-05-18 07:24:13', '2022-05-24 19:05:21'),
(2, 'App\\Models\\User', 2, 'auth_token', 'b2e4a047dca851d9f01e5a51e51684dd693e257d88b6cc28170b49b23d13bc95', '[\"*\"]', '2022-05-23 15:36:19', '2022-05-18 08:13:50', '2022-05-23 15:36:19'),
(3, 'App\\Models\\User', 3, 'auth_token', '68aadccf0b7c673a88cb6f61aefb1682d372a570e8dc4d6cb4b39228567aad55', '[\"*\"]', '2022-05-24 19:41:13', '2022-05-18 08:17:51', '2022-05-24 19:41:13'),
(4, 'App\\Models\\User', 4, 'auth_token', '81f3c2dacce406f9493155547c165866c254f00a10b24356dca288168025411c', '[\"*\"]', '2022-05-24 19:37:08', '2022-05-18 10:21:41', '2022-05-24 19:37:08'),
(5, 'App\\Models\\User', 5, 'auth_token', '9f22b2446f505c8b9d9fbe4256ab56f9ae14086c98197fb973ec061d7b9db0d4', '[\"*\"]', '2022-05-18 11:54:01', '2022-05-18 11:17:51', '2022-05-18 11:54:01'),
(6, 'App\\Models\\User', 6, 'auth_token', 'd922db1db64eb92480a9ee0c7037a43a580f356ee5394c8e91ac69fc11a3a912', '[\"*\"]', '2022-05-20 19:52:18', '2022-05-20 19:47:55', '2022-05-20 19:52:18'),
(7, 'App\\Models\\User', 7, 'auth_token', '8f177fc321e83730219aae5b39c5019689d5a807bad77663ab911729153f0383', '[\"*\"]', '2023-02-09 06:49:55', '2022-05-22 00:43:58', '2023-02-09 06:49:55'),
(8, 'App\\Models\\User', 8, 'auth_token', '5d644b883ddfd39dbd4030a8e2411dfc235941567100105814ed1972e7d2852a', '[\"*\"]', '2022-05-24 13:37:32', '2022-05-22 13:24:34', '2022-05-24 13:37:32'),
(9, 'App\\Models\\User', 9, 'auth_token', '3a58d383bbe2ecc802fba91f7a5fe0d3d066b6b09d6f8c2fb279c1b96449f8a4', '[\"*\"]', '2022-05-28 08:23:57', '2022-05-23 17:40:27', '2022-05-28 08:23:57'),
(10, 'App\\Models\\User', 10, 'auth_token', '42964a49bd5f4466a5a0bfe2e5b318ee1c3c3f391e83787621eb3968eb8d4ea7', '[\"*\"]', NULL, '2022-05-23 18:10:22', '2022-05-23 18:10:22'),
(11, 'App\\Models\\User', 11, 'auth_token', 'd50733b94afba60d6f815cec39b2b0335462b889d6cb67ab17ac7c6ea6b43b8e', '[\"*\"]', '2022-05-28 08:23:20', '2022-05-23 18:11:18', '2022-05-28 08:23:20'),
(12, 'App\\Models\\User', 12, 'auth_token', 'cc793d008f53f290aea210f026b6572f56eeef77929343f65e2e47b13a40d303', '[\"*\"]', NULL, '2022-05-23 18:13:02', '2022-05-23 18:13:02'),
(13, 'App\\Models\\User', 13, 'auth_token', 'cf035d3140d8190a230e79f8d351fc30d96e45f01f3cba435ee95b37aa979ebf', '[\"*\"]', NULL, '2023-01-31 13:25:11', '2023-01-31 13:25:11'),
(14, 'App\\Models\\User', 14, 'auth_token', '33a2993beae4237804d7b25dd123ada99e383fe7226acfdb0e6ad18df9756959', '[\"*\"]', '2023-01-31 15:30:36', '2023-01-31 13:29:53', '2023-01-31 15:30:36'),
(15, 'App\\Models\\User', 15, 'auth_token', '4d1a8b7b4b1fac770b8a54d2a4376e32595262dc7a60df8e16afa8e6f004e99e', '[\"*\"]', NULL, '2023-02-01 12:55:07', '2023-02-01 12:55:07'),
(16, 'App\\Models\\User', 16, 'auth_token', 'f23de380ee0575cd9130bfe265f699b10def57c0ec08f137d3236893b8867707', '[\"*\"]', NULL, '2023-02-01 16:27:29', '2023-02-01 16:27:29'),
(17, 'App\\Models\\User', 17, 'auth_token', '33a2ff228da64fe76606115590258b01d0680599787e2644c33e070dfa17ba00', '[\"*\"]', NULL, '2023-02-01 16:45:26', '2023-02-01 16:45:26'),
(18, 'App\\Models\\User', 18, 'auth_token', '1f188c5520b71afcf2676f4d238f4adfa487fbd4770c0a65a28d4e97f8545734', '[\"*\"]', NULL, '2023-02-01 16:50:44', '2023-02-01 16:50:44'),
(19, 'App\\Models\\User', 19, 'auth_token', 'ecf3c4d31ad036095682729961d398c604e2cb5ef4fd430e39be9386ed67b873', '[\"*\"]', NULL, '2023-02-01 16:54:31', '2023-02-01 16:54:31'),
(20, 'App\\Models\\User', 20, 'auth_token', 'dd79ad8ed66de0c6f745d025656bb6500e64983e7990fee61418116ade0881fa', '[\"*\"]', '2023-02-02 04:59:48', '2023-02-01 16:57:01', '2023-02-02 04:59:48'),
(21, 'App\\Models\\User', 21, 'auth_token', '51bfd59c3f06fbd9116f1759b248b805983bde580511b300e3323b44a2cd1dd1', '[\"*\"]', NULL, '2023-02-02 02:34:10', '2023-02-02 02:34:10'),
(22, 'App\\Models\\User', 22, 'auth_token', 'd15bc7876ff5a26d11bb73f90a8a76a35ab2e21336caf39f061df8b3afe7ccc6', '[\"*\"]', '2023-02-09 08:24:41', '2023-02-02 08:25:48', '2023-02-09 08:24:41'),
(23, 'App\\Models\\User', 23, 'auth_token', '005e39b6b646dbf1e27f378c96316c6b0ba9b7688191683d84a9cfc23a0ec451', '[\"*\"]', NULL, '2023-02-02 08:49:44', '2023-02-02 08:49:44'),
(24, 'App\\Models\\User', 25, 'auth_token', '3816a7162f118e14d40ad80b7745a69053392f1fc6f5aa491bcf535b62d47125', '[\"*\"]', NULL, '2023-02-07 04:04:48', '2023-02-07 04:04:48'),
(25, 'App\\Models\\User', 43, 'auth_token', '90e32a8578bdac6f7f32ccafabd848ab61b2052aaded67bd560de463a9b05dca', '[\"*\"]', NULL, '2023-02-07 07:53:59', '2023-02-07 07:53:59'),
(26, 'App\\Models\\User', 69, 'auth_token', 'f1015434c0c690da9dde7b0e8e77ac1870de159b3e61fe127e4393bc6e740181', '[\"*\"]', '2023-02-10 06:41:28', '2023-02-10 03:33:54', '2023-02-10 06:41:28');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id_pro` int(10) UNSIGNED NOT NULL,
  `name_pro` varchar(255) NOT NULL,
  `phanloai` varchar(255) NOT NULL,
  `mota` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `icon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id_pro`, `name_pro`, `phanloai`, `mota`, `created_at`, `icon`) VALUES
(30, 'Product A', 'Product 1', 'Product 1', NULL, 'laravel/basic_cmss/public/images/1672110261.png'),
(31, 'Product B', 'Product 1', 'Product 1', NULL, 'laravel/basic_cmss/public/images/php.png'),
(32, 'Product C', 'Product 2', 'Product 2', NULL, 'laravel/basic_cmss/public/images/1672110261.png'),
(33, 'Prod ABC', 'ABC', '123', NULL, 'laravel/basic_cmss/public/images/1675670429.png'),
(36, 'Product A new', 'Product  new', 'Product new', NULL, 'laravel/basic_cmss/public/images/1675327995.png'),
(38, 'Prod ABC v2 new', 'product1 new', 'product1 new v2', NULL, 'laravel/basic_cmss/public/images/1675307714.png'),
(72, 'Docker', '123', 'undefined', NULL, 'laravel/basic_cmss/public/images/php.png'),
(75, 'C product', 'C product', 'C product', NULL, 'laravel/basic_cmss/public/images/2.png'),
(88, 'Python v3', 'Python v3', 'Python v3', NULL, 'laravel/basic_cmss/public/images/1675306229.jpg'),
(99, 'hehehe 3', 'hehehe', 'hehehe', NULL, 'laravel/basic_cmss/public/images/1675307714.png'),
(104, '1 tesst', '1 test', 'test', NULL, 'laravel/basic_cmss/public/images/1675306229.jpg'),
(106, 'product', 'product', 'product', NULL, 'laravel/basic_cmss/public/images/1672110261.png'),
(107, 'dang ky product v2', 'prod', '123', NULL, 'laravel/basic_cmss/public/images/1675327995.png');

-- --------------------------------------------------------

--
-- Table structure for table `pro_details`
--

CREATE TABLE `pro_details` (
  `id_prodetail` int(11) UNSIGNED NOT NULL,
  `link` varchar(111) NOT NULL,
  `userpass` varchar(111) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `image` varchar(1111) NOT NULL,
  `version` varchar(4) NOT NULL,
  `active` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pro_details`
--

INSERT INTO `pro_details` (`id_prodetail`, `link`, `userpass`, `product_id`, `created_at`, `image`, `version`, `active`) VALUES
(39, 'details ABC f2 v', 'userpass ABC v2', 30, NULL, '/laravel/basic_cmss/public/images/1675306229.jpg', 'dev', 1),
(59, 'details  ABC', 'userpass ABC', 30, NULL, '/laravel/basic_cmss/public/images/1675269790.png', 'prod', 1),
(60, 'DetialsAAA new', 'DetialsAAA', 33, NULL, '/laravel/basic_cmss/public/images/1675309231.png', 'prod', 1),
(65, 'Link Reactjs', 'userpass react', 38, NULL, '/laravel/basic_cmss/public/images/1675309179.jpg', 'dev', 1),
(66, 'DetialsAAA new', 'DetialsAAA', 38, NULL, 'download.jpg', 'prod', 1),
(67, 'link 1', 'pass', 36, NULL, '1671443977.png', 'dev', 1),
(68, 'DetialsAAA new v2', 'DetialsAAA', 36, NULL, '1671444002.jpg', 'prod', 1),
(84, 'version dev', 'version dev', 83, NULL, '1673423369.jpg', 'dev', 1),
(93, 'version prod', 'version prod', 83, NULL, '1673424092.jpg', 'prod', 1),
(104, '32 _ Detail', '32 _ Detail', 32, NULL, '1675306229.jpg', 'dev', 0),
(106, '32_ PROD', '32_ PROD', 32, NULL, '1675307759.jpg', 'prod', 0),
(109, 'React_Dev', 'React_Dev', 32, NULL, '1675309231.png', 'dev', 1),
(110, 'new Pro details', 'DEV new Pro details', 93, NULL, '/laravel/basic_cmss/public/images/1672110261.png', 'dev', 1),
(111, 'Prodnew Pro details', 'Prodnew Pro details', 93, NULL, '/laravel/basic_cmss/public/images/1675257208.jpg', 'prod', 1),
(121, '123', '123', 99, NULL, '/laravel/basic_cmss/public/images/2.png', 'dev', 0),
(122, 'de v3', 'v3', 99, NULL, '/laravel/basic_cmss/public/images/1675306229.jpg', 'dev', 1),
(124, '123', '123', 99, NULL, '/laravel/basic_cmss/public/images/1675306229.jpg', 'prod', 0),
(125, '123', '123', 99, NULL, '/laravel/basic_cmss/public/images/1675309199.jpg', 'prod', 0),
(126, '123', '123', 99, NULL, '/laravel/basic_cmss/public/images/1675269790.png', 'prod', 1),
(127, '123', '123', 104, NULL, '/laravel/basic_cmss/public/images/1672110261.png', 'dev', 0),
(128, '123', '123', 104, NULL, '/laravel/basic_cmss/public/images/1675306229.jpg', 'dev', 0),
(129, '22', '2', 104, NULL, '/laravel/basic_cmss/public/images/1675269790.png', 'dev', 1),
(130, '22', '2', 104, NULL, '/laravel/basic_cmss/public/images/1675669201.png', 'prod', 0),
(131, '33', '33', 104, NULL, '/laravel/basic_cmss/public/images/1672110261.png', 'prod', 1),
(132, 'link 1', 'pass', 107, NULL, '/laravel/basic_cmss/public/images/1675327901.png', 'prod', 1),
(133, 'link pro', 'pass dev', 107, NULL, '/laravel/basic_cmss/public/images/1675326569.png', 'dev', 0),
(134, 'dev v2', 'dev v2', 107, NULL, '/laravel/basic_cmss/public/images/1672110261.png', 'dev', 1);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'writter', 'api', NULL, NULL),
(2, 'publish', 'api', NULL, NULL),
(4, 'admin2', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03'),
(5, 'admin3', 'api', '2022-12-19 20:49:09', '2022-12-19 20:49:09'),
(6, 'publisher', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03'),
(7, 'clientdev', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03'),
(8, 'clientprod\r\n', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03'),
(9, 'writter\r\n', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03'),
(10, 'editter', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03'),
(11, 'admin', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03'),
(12, 'super_admin', 'api', '2022-12-19 20:47:03', '2022-12-19 20:47:03');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 4),
(2, 1),
(2, 5),
(4, 1),
(5, 1),
(5, 5),
(6, 7),
(6, 11),
(7, 7),
(7, 8),
(8, 6),
(8, 9),
(8, 11),
(8, 12),
(9, 10),
(9, 11),
(9, 12),
(10, 11),
(10, 12);

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seats` int(11) DEFAULT NULL,
  `private_room` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `name`, `address`, `phone`, `seats`, `private_room`, `created_at`, `updated_at`) VALUES
(1, 'Chay Xanh Quận 2', '50 Lê Văn Thịnh, Phường Bình Trưng Đông, Quận 2, Thành Phố Thủ Đức', '0365003601', NULL, NULL, '2022-05-18 07:34:58', '2022-05-18 07:34:58'),
(2, 'Chay Xanh Gò Vấp', '12 Nguyễn Văn Bảo, Phường 4, Gò Vấp, Thành phố Hồ Chí Minh', '02838940390', NULL, NULL, '2022-05-18 07:36:53', '2022-05-18 07:36:53'),
(3, 'Chay Xanh Quận 1', '185/16 Phạm ngũ Lão, Quận 1', '0365003601', NULL, NULL, '2022-05-18 07:49:16', '2022-05-18 07:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` smallint(6) DEFAULT NULL,
  `store_id` smallint(6) DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phanquyen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `username`, `access_token`, `role_id`, `store_id`, `phone`, `address`, `phanquyen`) VALUES
(7, 'admin', 'adminv2@gmail.com', NULL, '$2y$10$QaCJNX1grwZWL5mkL5iuYu/yFjDcAhDGe.hB0ZM1qwp1Rkz3H4kCC', NULL, '2022-05-22 00:43:58', '2023-02-09 08:22:51', 'admin', '7|jijhZ5Cp7jGl2YRFyVU8JIPQ6NL5tWQsBmzQ1Kzv', 1, 1, '0360000005', '123', 0),
(8, 'quanlyquan2', 'quanlyquan2@gmail.com', NULL, '$2y$10$YeB1ZccfDrqLMkmbUq9RpO2CHgHDoG1IZbEsmYJqcYHsRnzLNiNAO', NULL, '2022-05-22 13:24:34', '2022-05-22 13:24:34', 'ql2', '8|xOixB3wDIdMvJfMztQNOlS99O7jqHck5vB8KF292', 2, 1, '0110000001', NULL, 0),
(20, 'User Duy', 'new12123@gmail.com', NULL, '$2y$10$qBFwB5pW.qItgTiQZFzUbu7MmORcFyB0bD94fC8FgP65BLPjhJ7TW', NULL, '2023-02-01 16:57:01', '2023-02-02 03:41:34', 'new12', '20|TWeXJTUQHtVCiVNBVafyYJDs4g9O6qRZ7l47clXP', 11, NULL, '1', NULL, 0),
(21, 'duy test 2', 'duytest@gmail.com', NULL, '$2y$10$kIs7Lr5LovDrUUqWLSPhn.Wc0h3g2IYOr2tnslf/eWK0WvQLgzDzq', NULL, '2023-02-02 02:34:10', '2023-02-02 02:47:28', 'duytestttt', '21|b1xhcP0eEc0VSi5jwNKtmAwlpVc0izASSpFUZdld', NULL, NULL, '1231233333', NULL, 0),
(22, 'duyuser', 'duytest2@gmail.com', NULL, '$2y$10$Ff9DQYzzoe1c8KWIWcvKru2Ettj7DmT2HcCv31RO7IgJD9DJ9hqAq', NULL, '2023-02-02 08:25:48', '2023-02-09 04:26:00', 'duyuser', '22|ep1oj7RRS9tbFpDS3qJ6V8holy4pT8i9JPUJplcn', 1, NULL, '0011', NULL, 0),
(23, 'Version 2', 'duytest3@gmail.com', NULL, '$2y$10$6DLovPgLDZHRVSXAcl42DOuQY6A7tO62Q1Fv565ZrcdSco6WzO54C', NULL, '2023-02-02 08:49:44', '2023-02-08 07:47:44', 'duytest2', '23|ymGSRwzMaPp5fzRAEQTAA8ZgPOqHFNZRumjSM5cq', 11, NULL, '1223', NULL, 0),
(43, 'Đinh Văn Duy v1', 'dinhvanduy0503@gmail.com', NULL, '$2y$10$otgsrcdXmNk2l/R73eOv5eduPR6NT6civS/W7kiWZ5aIBuRyKGsn2', NULL, '2023-02-07 07:53:59', '2023-02-09 03:14:09', '', '25|CwEJ6l79cjDCSt3qlQzDNtlthzV1niPbTC33p2jA', 0, NULL, '', NULL, 0),
(69, 'testdangky', NULL, NULL, '$2y$10$tjqESMi6KwDHhI3s5dU04.72amvLeg55OxyVRQ/ONun.ZrAXw8Ifm', NULL, '2023-02-10 03:33:54', '2023-02-10 03:41:19', 'testdangky', '26|WjGbkp0nx36ohCqpNcANAMfShxsx7Rl0BT3EeZAR', 11, NULL, '12312355454', NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_pro`);

--
-- Indexes for table `pro_details`
--
ALTER TABLE `pro_details`
  ADD PRIMARY KEY (`id_prodetail`),
  ADD KEY `active` (`active`),
  ADD KEY `active_2` (`active`),
  ADD KEY `active_3` (`active`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`) USING BTREE,
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id_pro` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `pro_details`
--
ALTER TABLE `pro_details`
  MODIFY `id_prodetail` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
