import { Avatar, Badge, Button, Drawer, Dropdown, Empty, Layout, Menu } from 'antd';
import React, { useContext, useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import { UserOutlined, DownOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import { AppContext } from '../store';
import ItemCart from './components/ItemCart';

const { Header, Content, Footer } = Layout;

const Client = () => {
  const { user, cart, money, cart_length } = useContext(AppContext);
  const [drawer, setDrawer] = useState(false);

  const onClose = () => {
    setDrawer(false);
  };

  const menu = (
    <Menu>
      {user.role_id ?
        <Menu.Item key="0">
          <Link to="/admin">CMS</Link>
        </Menu.Item> : ''}
      <Menu.Item key="1">
        <Link to="/me">Thông tin cá nhân</Link>
      </Menu.Item>
     
      <Menu.Item key="3">
        <Link to="/logout">Log out</Link>
      </Menu.Item>
    </Menu>
  );

  return (
    <>
      <Layout className="layout" style={{ minHeight: '100vh' }}>
        <Header style={{ position: 'fixed', zIndex: 1, width: '100%', background: 'rgba(250,250,250,0.8)' }}>
          <div className="login">
            {!user.id
              ? <Button type='primary'><Link to='/login'>Login</Link></Button>
              : <>
                {user.name}
                <Dropdown overlay={menu} trigger={['click']}>
                  <DownOutlined />
                </Dropdown>
              </>}
          </div>
          <div style={{ float: 'right', padding: '4px 20px' }} onClick={() => setDrawer(!drawer)}>
            
          </div>
          <Menu style={{ background: 'rgba(250,250,250,0)' }} mode="horizontal" defaultSelectedKeys={['home']}>
            <Menu.Item key="home">
              <Link to='/'>Trang chủ</Link>
            </Menu.Item>
            <Menu.Item key="cient">
              <Link to='/client'>Client View</Link>
            </Menu.Item>
            <Menu.Item key="Cms">
              <Link to='/admin'>CMS</Link>
            </Menu.Item>
           
          </Menu>
        </Header>

        <Content style={{ marginTop: '63px' }}>
          <Outlet />
        </Content>

        <Footer style={{ textAlign: 'center' }}>DuyProject</Footer>
       
   
       
      </Layout>
    </>
  )
}

export default Client