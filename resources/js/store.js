import axios from "axios";
import { createContext, useEffect, useReducer } from "react";
const initialState = {
  user: JSON.parse(localStorage.getItem("user")) ? JSON.parse(localStorage.getItem("user")) : { role_id: '' },
  cart: [],
  cart_length: 0,
  money: 0,
};
const actions = {
  SET_USER: "SET_USER",
  SET_PRODUCT: " SET_PRODUCT",
  SET_PRODUCTDETAILS: " SET_PRODUCTDETAILS"
};

const reducer = (state, action) => {
  switch (action.type) {
    case actions.SET_USER:
      return {
        ...state,
        user: action.user,
      }
    case actions.SET_PRODUCT:
      let sumTemp = 0
      let sumpLength = 0
      state.cart.map(item => {
        sumTemp += item.price * item.qty
        sumpLength += item.qty
      })
      return { ...state, money: sumTemp, cart_length: sumpLength }

  }
};

export const AppContext = createContext();

const Provider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const value = {
    user: state.user,
    users: state.users,
    setUser: (user) => {
      dispatch({ type: actions.SET_USER, user });
    },
    cart: state.cart,
    money: state.money,
    cart_length: state.cart_length,
    addCart: (data) => {
      dispatch({ type: actions.ADD_CART, data });
      dispatch({ type: actions.SUM_MONEY });
    },
    minusCart: (data) => {
      dispatch({ type: actions.MINUS_CART, data });
      dispatch({ type: actions.SUM_MONEY });
    },
    resetCart: () => {
      dispatch({ type: actions.RESET_CART });
    },
    
    //Client 
    getItemsClient:() =>{
      return axios.get(`/api/productclient`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    getItemsDev:() =>{
      return axios.get(`/api/productclient/dev`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    getItemsProd:() =>{
      return axios.get(`/api/productclient/prod`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },

    // Me
    getMe: () => {
      return axios.get(`/api/me`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    updateMe: (values) => {
      return axios.post(`/api/me/update`, values, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },

    dashboard: (current) => {
      console.log("current in store",current)
      if(current != 1 ){
      return axios.post(`/api/me/dashboard?page=${current}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
      }
      return axios.post(`/api/me/dashboard`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },

    deleteUser2:(id) =>{
      console.log("put",id)
      return axios.put(`/api/me/dashboard/putUser/${id}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },
    getProduct:(current) =>{
      if(current>= 2){
        console.log(">= 2",current)
        return axios.post(`/api/me/product?page=${current}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })


      }
      else {
        console.log("==1")
        return axios.post(`/api/me/product?page=1`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
      }
    },
 


    getProductDetails:(current) =>{
      if(current >= 2){
      return axios.post(`/api/me/productdetails?page=${current}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
      }
      return axios.post(`/api/me/productdetails?page=1`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },


    //ME /Action/User

    getEditUser:(id_pro) =>{
      return axios.get(`/api/me/edituser/${id_pro}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },

    putEditUser:(id_pro,inputs) =>{
      return axios.put(`/api/me/putedituser/${id_pro}`, inputs, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },

    putRole:(id, inputs) =>{
      return axios.put(`/api/me/editrole/${id}`,inputs, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    getEditPro:(id_pro) =>{
      return axios.get(`/api/me/editProduct/${id_pro}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },

    putEditPro:(id_pro,inputs) =>{
      return axios.put(`/api/me/putProduct/${id_pro}`,inputs, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },


    
    addPro:(inputs) =>{
      return axios.post(`/api/me/addPro`,inputs, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },

    //===========

    editDetails:(id_prodetail) =>{

      return axios.get(`/api/me/getdetails/${id_prodetail}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },
    putDetails:(id_prodetail,inputs) =>{
      return axios.put(`/api/me/putDetails/${id_prodetail}`,inputs, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },
    deleteDetails:(id_prodetail) =>{
      return axios.delete(`/api/me/deleteDetails/${id_prodetail}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },


    deletePro:(id_prodetail) =>{
      return axios.delete(`/api/me/deletePro/${id_prodetail}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },

  
    addDetails:(formDaTa) =>{
      return axios.post(`/api/me/addDetails`,formDaTa, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })

    },


    //==
    //Users
    getListUsers: () => {
      return axios.get('/api/users/getlist', { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    getUserByID: (id) => {
      return axios.get(`/api/users/${id}`, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    storeUser: (values) => {
      return axios.post(`/api/users`, values, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    updateUser: (values) => {
      return axios.post(`/api/users/update`, values, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    deleteUser: (id) => {
      return axios.post(`/api/users/delete/${id}`, '', { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    getListUsersMG: (values) => {
      return axios.post(`/api/users/getlistManager`, values, { headers: { "Authorization": `Bearer ${state.user.access_token}` } })
    },
    
    
  };

  return (
    <AppContext.Provider value={value}>
      {children}
    </AppContext.Provider>
  );
}

export default Provider