import React, { useContext } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Provider from './store';
import 'antd/dist/antd.less';
import '../css/app.css';
import { AppContext } from './store';
//
import Client from './Client/Client';
import Home from './Client/Home';
import Login from './Client/Login';
import Register from './Client/Register';
import Cms from './Cms/Cms';
import Logout from './Logout';
import Users from './Cms/Users/Users';
import Me from './Duy_component/me'; 
import Dashboard from './components/Dashboard'
import EditUser from './components/EditUser';
import AllProduct from './components/Allproduct';
import AllProductdetails from './components/Allproductdetails';
import EditRole from './components/editRole';
import Editprod from './components/Editprod';
import AddDetail from './components/AddDetails';
import Auth_Client from './navbar/auth_client';
import CreatePro from './components/CreateProd'
import Editdetails from './components/Editdetails';


function App() {
  function PrivateOutlet() {
    const { user } = useContext(AppContext);
    const auth = user.role_id ? user.role_id : false;
    const id_role = user.role_id;
    return id_role == 1 ? <Cms/> : <Navigate to="/" />;
  }
  return (
    <div className='App'>
      <Routes>
            <Route path='/' element={<Client />}>
            <Route index element={<Home />} />
            <Route path='/me' element={<Me />} />
            <Route path='/login' element={<Login />} />
            <Route path='/register' element={<Register />} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/client' element={<Auth_Client />} />
        </Route>

        <Route path='/admin' element={<PrivateOutlet />}>
            <Route index element={<Dashboard />} />
            <Route path='users' element={<Users />}/>
            <Route path='trash' element={<Users />}/>
            <Route path='product' element={<AllProduct />}/>
            <Route path='details' element={<AllProductdetails />}/>
            <Route path='editUser/:id_pro' element={<EditUser />} />
            <Route path='role_ver/:id' element={<EditRole />} />
            <Route path='product/Editprod/:id_pro' element={<Editprod />} />
            <Route path='product/CreateDetails/:id' element={<AddDetail />} />
            <Route path='details/Editdetails/:id_pro' element={<Editdetails />} />
            <Route path="product/CreatePro" element={<CreatePro />} />
        </Route>
      </Routes>
    </div>
  );
}

ReactDOM.render(
  <BrowserRouter>
    <Provider>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById('app')
)