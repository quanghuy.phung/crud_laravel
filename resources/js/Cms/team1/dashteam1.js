import React from 'react';
import {Carousel ,Anchor, Col, Row,  Image} from 'antd';
import { Card } from 'antd';
import { auto } from '@popperjs/core';
const { Meta } = Card;
const contentStyle = {
  height: '160px',
  color: '#fff',
  lineHeight: '160px',
  textAlign: 'center',
  background: '#364d79',
};
const gridStyle = {
  width: '25%',
  textAlign: 'center',
};
const DashTeam1 = () => {
  return (
    <>
    <Row>
      <Col span={24}>

          <Carousel autoplay>
        <div>
          <h3 style={contentStyle}>
          <Image
                    width={1000}

          height={160}

          src="https://cdn0.fahasa.com/media/magentothem/banner7/FahasaT1_mainbanner_01_Slide_840x320.jpg"
        />
          </h3>
        </div>
        <div>
          <h3 style={contentStyle}>
          <Image
                    width={1000}

          height={160}
          src="https://cdn0.fahasa.com/media/magentothem/banner7/HoaCuQuy4_banner_840x320_Reup.jpg"
        />
          </h3>
        </div>
        <div>
          <h3 style={contentStyle}>
          <Image
          width={1000}
          height={160}

          src="https://cdn0.fahasa.com/media/magentothem/banner7/VPP_Main_banner_T10_840x320.jpg"
        />
          </h3>
        </div>
        <div>
          <h3 style={contentStyle}>4</h3>
        </div>
      </Carousel>
      </Col>
    </Row>
    <Row>
      <Col span={12}>
     
      </Col>
      <Col span={12}>col-12</Col>
    </Row>
    <Row>
      <Col span={8}>col-8</Col>
      <Col span={8}>col-8</Col>
      <Col span={8}>col-8</Col>
    </Row>
    <Row>
      <Col span={6}>col-6</Col>
      <Col span={6}>col-6</Col>
      <Col span={6}>col-6</Col>
      <Col span={6}>col-6</Col>
    </Row>
  </>
  )
}
export default DashTeam1;