import React, { useState , useEffect  } from 'react';
import { Button, Table, Space,Modal ,Spin, Image,FloatButton} from 'antd';
import http from "../http";
import { DeleteOutlined , LeftSquareOutlined ,LeftOutlined,RedoOutlined} from '@ant-design/icons';

import { useNavigate } from "react-router-dom";
import { Col, Drawer, Form, Input, Row, Select } from 'antd';
import { colors } from 'laravel-mix/src/Log';
const { Option } = Select;
const initPagination = {
  current: 1,
  defaultCurrent: 1,
  total: 20,
  defaultPageSize: 4,
  showSizeChanger: false
}
var someValue = window.sessionStorage.getItem('user');
var obj = JSON.parse(someValue); 

const Trash = () => {
const navigate = useNavigate(); 

const thungrac =() =>{
  navigate('/ThungRac')
}
const [loading2 , setLoading2] = useState(false)
const [open, setOpen] = useState(false);
const showDrawer = () => {
  setOpen(true);
};
const onClose = () => {
  setOpen(false);
};
const [form] = Form.useForm();
  const [current, seturlPage] = useState([]);
  const [product22, setProduct] = useState([]);
  const [users, setUsers] = useState([]);

  const [pagination, setPaginate2] = useState(initPagination);
  const sumbitXem = (id_pro) =>{
    console.log(id_pro)
    form.setFieldsValue(id_pro)
    showDrawer(); 
  }
  //lấy id trên params
 
  const handleBack = () =>{
    navigate('/allproductdetails')
  }
  const handletablechange2 = (pagination) => {

    console.log(pagination.current);
    let p = {
      current: pagination.current,
      defaultCurrent: 1,
      total: 7,
      defaultPageSize: 4,
      showSizeChanger: false
    };
    setPaginate2({...p})
    fetchAllProdetails(pagination.current);
  };
  const editdetails = (id_prodetail) =>{
    console.log("id_prodetail",id_prodetail.id_prodetail)
    http.put('/trash/'+id_prodetail.id_prodetail).then((res)=>{


        navigate('/ThungRac');
        fetchAllProdetails()
    })  
  editdetails
  }
  
  const deleteDetails = (id_prodetail) =>{
    console.log("id_prodetail",id_prodetail.id_prodetail)
    http.delete('/trash/'+id_prodetail.id_prodetail).then((res)=>{
        navigate('/ThungRac');
        fetchAllProdetails()
    })  
  editdetails
  }
  const [id_dlte,setDLte] = useState([])

  const deleteProduct = (id_prodetail) => {
    setIsModalOpen(true);
    console.log(id_prodetail.id_prodetail)
  }
  //MODAL DELETE
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = ({id_prodetail}) => {
    console.log(id_prodetail)
    http.delete('/productdetails/'+id_prodetail).then(res=>{
      setIsModalOpen(false);
      
      fetchAllProdetails();
    })
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  
  const columns1 = [
    {
      title: 'Hình  ',
      dataIndex: 'image',
      render: (_, record) => {
        return (
          <Image src={"http://localhost/laravel/reactjsnew/hehehe/public/images/"+record.image} width="50px" height={50}/>
        );
      }
    },
    {
      title: 'Phân Loại',
      dataIndex: 'link',
    },
    {
      title: 'Mô tả',
      dataIndex: 'userpass',
    },
    {
      title: 'Phiên Bản',
      dataIndex: 'version',
    },
    {
      title: 'Hành Động',
      key: 'action',
      render: (id_pro, record) => (
        <Space size="middle">
          {/* <a onClick={()=>{editdetails(id_pro)}}>Edit</a>
          <a onClick={()=>{deleteProduct(id_pro)}} >Delete</a>
          <Modal title="Delete" open={isModalOpen} onOk={() => {handleOk(id_pro)}} onCancel={handleCancel}>
          <p> Bạn Chắc Chắn Muốn Xóa ! </p>
          </Modal> */}
        <a  className='restore' onClick={()=>{editdetails(id_pro)}}><RedoOutlined style={{ fontSize: '20px' }} /></a>
        <a  className='deletedetail' onClick={()=>{deleteDetails(id_pro)}}><DeleteOutlined style={{ fontSize: '20px' }}  /></a>



          </Space>
      ),
    },
  ];
  // bắt đầu phân trang
  useEffect(()=>{
    fetchAllProdetails();
  },[]);
  // end phân trang
  const fetchAllProdetails = (current) => {
    setLoading2(true)
    http.get('/trash'+'?page='+current).then(res=>{
        setProduct(res.data);
        setLoading2(false)
    })
}

  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const start = () => {
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      
    }, 1000);
  };
  const onSelectChange = (newSelectedRowKeys) => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
    return (
      <div>
        <div
          style={{
            marginBottom: 16,
          }}
         >
          <span
            style={{
              marginLeft: 8,
            }}
          >
            {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
          </span>
        </div>
        <Drawer
          title="INFORMATION"
          width={720}
          onClose={onClose}
          open={open}
          bodyStyle={{
            paddingBottom: 80,
          }}
          extra={
            <Space>
              <Button onClick={onClose}>Cancel</Button>
              <Button onClick={onClose} type="primary">
                Submit
              </Button>
            </Space>
          }
        >
          <Form form={form} layout="vertical">
          <Image
              width={150}
              src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRC6iPDSqcgCcAtdEz_rPY0B-sxqMd7hz0Hlg&usqp=CAU"
            />
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  name="image"
                  label="Hinh Anh"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
               <Image
               width={200}
               src=""
               />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="phanloai"
                  label="Phân Loại"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="mota"
                  label="Mô tả"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter user name',
                    },
                  ]}
                >
                  <Input placeholder="Please enter user name" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="link"
                  label="Link"
                  rules={[
                    {
                      required: true,
                      message: 'Please enter url',
                    },
                  ]}
                >
                  <Input
                    style={{
                      width: '100%',
                    }}
                    addonBefore="http://"
                    addonAfter=".com"
                    placeholder="Please enter url"
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Drawer>
        <Spin spinning={loading2}>
            <h2>TRASH</h2>
                <a className='back' onClick={handleBack} > 
                < LeftSquareOutlined className='iconback' />
            Quay Về

                </a> 
        <FloatButton className='backicon' icon={<LeftOutlined />} onClick={() => handleBack()} />

        <Table rowSelection={rowSelection} columns={columns1} onChange={handletablechange2} 
         pagination={pagination}  dataSource={product22} rowKey={"id_pro"}/>
            </Spin>
      </div>
    );
  
};
export default Trash; 