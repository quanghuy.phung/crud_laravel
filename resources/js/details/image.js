import {Popover,Image , Tabs,Button, Col} from 'antd'
import PROD from '../details/prod';
import DEV from '../details/dev';
export default function Image222 ({items,index}){

  
  
const content =()=> (
    
  <div>
    <Tabs
        defaultActiveKey="1"
        onChange={onchange}
        items={[
          {
            label: `PROD`,
            key: '1',
            children:<PROD items={items}/>,
          },
          {
            label: `DEV`,
            key: '2',
            children: <DEV items= {items}/>,
          },
          
        ]}
    />
  </div>
      )
    return(
      <Col>
        <div className='Icon' key={index} > 
        <Popover content={content}  trigger="click" title="Information" >
            <Image preview={false}  src={"http://localhost/"+items.icon}
            width={50} height={50}/>
        </Popover>
        </div>
      </Col>
    )
}