import React ,{useEffect , useContext} from 'react';
import { Card, Col, Row, Image , Spin} from 'antd';
import { AppContext } from '../store';
import http from '../http';
import { useState } from 'react';
export default function PROD({items}){
  const [loading2 , setLoading2] = useState(false)
  const {getItemsProd} = useContext(AppContext)
  const [getDev , setGetDev] = useState([])
  useEffect(()=>{
    getItemsProd().then((res)=>{
      let data = res.data.filter(index =>(index.version == "prod") )
      let data2 = data.filter(index2=>index2.product_id == items.id_pro)
      console.log("data2",data2);
      let checkData = data2;
      setGetDev([...data2])
      setLoading2(false)
    })
  },[]);
    return( 
      <Spin spinning={loading2}>
      <div className="site-card-wrapper">
      <Row gutter={6} style={{width:"400px", height:"300px"}}> 
      {
       getDev.map((data, index)=>{
        if(data === null){
          console.log("null")
        }
       return (
        <Col span={12}  key={index} >
          <Card title="Prod" bordered={true}>
          <ul className='list-group'>
              <Image src={"http://localhost/"+ data.image} width={40} height={40}/>

            <li  className="list-group-item" > {data.link}</li>
            <li  className="list-group-item">{data.userpass}</li>
            {/* <li  className="list-group-item"> {data.product_id}</li> */}
          </ul>
          </Card>
        </Col>
       )
       })
      }
       
      </Row>
    </div>
    </Spin >

    )
  

  
}