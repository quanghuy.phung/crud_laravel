import React,{ useState , useEffect}from 'react';
import { Breadcrumb, Layout, Menu, theme, Popover , Tabs, Spin,Popconfirm,message } from 'antd';
import { Image } from 'antd';
import { Col, Divider, Row} from 'antd';
import http from '../http';
import {
  UserOutlined,
} from '@ant-design/icons';
import AuthUser
 from '../components/AuthUser';
import Image222 from '../details/image';
const confirm = (e) => {
  console.log(e);
  message.success('Đăng Xuất Thành Công');
};
const cancel = (e) => {
  console.log(e);
  message.error('Hủy');
};
const { Header, Content, Footer } = Layout;
const Auth_Client = () => {
  const [loading2 , setLoading2] = useState(false)
  var someValue = window.sessionStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var email = obj.email;
  const {
    token: { colorBgContainer },
  } = theme.useToken();
const [icon, setIcon] = useState([]);
const [getID,setId_pro] =  useState([]);
const [details, setDetails] =  useState([]);
const {token,logout} = AuthUser();
const logoutUser = () => {
    if(token != undefined){
        logout();
    }
} 
  useEffect(()=>{
    fetchAllIcon();
  },[]);
  const fetchAllIcon = ()=>{
    setLoading2(true)
    http.get('/productclient').then(res=>{
      setIcon(res.data.data)
      res.data.data.map((items)=>{
      setId_pro(items.id_pro)
    })
    setLoading2(false)

    })
  }
  return (
    <div>   
    <Layout>
        <Header
          style={{
            position: 'sticky',
            top: 0,
            zIndex: 1,
            width: '100%',
          }}
        >

          <div
            style={{
              float: 'left',
              width: 120,
              height: 31,
              margin: '16px 24px 16px 0',
              background: 'rgba(255, 255, 255, 0.2)',
            }}
          />
    <Row justify="start">
          <Col span={19}>
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={['2']}
            items={new Array(3).fill(null).map((_, index) => ({
              key: String(index + 1),
              label: `MANAGE USER ${index + 1}`,
            }))}
          />
          </Col>
          <Col  span={5}> 
            <p className='chu'>
              Hello! {email}
              <Popconfirm
              title="Đăng Xuất !!"
              description="Bạn có chắc đăng xuất"
              onConfirm={logoutUser}
              onCancel={cancel}
              okText="Yes"
              cancelText="No"
            >
                <UserOutlined  className='icon'/>
            </Popconfirm>
            </p>
          </Col>
        </Row>
        </Header>
        <Content
          className="site-layout"
          style={{
            padding: '0 50px',
          }}
        >
          <Breadcrumb
            style={{
              margin: '16px 0',
            }}
          >
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
            <Breadcrumb.Item>App</Breadcrumb.Item>
          </Breadcrumb>
          <div
            style={{
              padding: 24,
              minHeight: 380,
              background: colorBgContainer,
            }}
          >
                  <Spin spinning={loading2}>
          {
              icon.map((items, index)=>{
                  return (
                     <Image222 items= {items} index={index} />

                  )
              }
              )
          }
                  </Spin>

          </div>
        </Content>
        <Footer
          style={{
            textAlign: 'center',
          }}
        >
          Ant Design ©2018 Created by Ant UED
        </Footer>
      </Layout>

    </div>
  );
};
export default Auth_Client;