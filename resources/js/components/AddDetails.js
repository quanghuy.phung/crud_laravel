import Axios from "axios";
import { useEffect, useState , useContext  } from "react";
import { useNavigate,useParams } from "react-router-dom";
import { UploadOutlined } from '@ant-design/icons';
import { AppContext } from "../store";
import React from 'react';
import { Button, Form, Input, Select } from 'antd';
import {  message, Upload } from 'antd';
import http from "../http";
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16
  },
};
/* eslint-disable no-template-curly-in-string */
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
/* eslint-enable no-template-curly-in-string */

const AddDetails = () => {
const [form] = Form.useForm();
const {addDetails} = useContext(AppContext)
const navigate = useNavigate();
const {id} = useParams([]);

form.setFieldValue("product_id",id)
const [inputs,setInputs] = useState({});
const [imagedata, setImagedata] = useState({});
const [file, setFile] = useState({});

const [oldData , setOldata] = useState([])
  const handleChangeimage = (file) =>{
      setImagedata(file[0]);
    };
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}

useEffect(()=>{
  fetchOneByid();

},[]);

const normFile = (e) => {
  if (Array.isArray(e)) {
    return e;
  }
  return e && e.fileList;
};

const dummyRequest = ({ file, onSuccess }) => {
  setTimeout(() => {
    onSuccess("ok");
  }, 0);
};


const fetchOneByid=()=>{
  http.get('/CreateDetails/'+id )
  .then(res=>{

    datafil =res.data.map(index=>{
      setOldata({
        link:index.link,
        version:index.version,
        userpass:index.userpass,
      })
    })
  })
}
form.setFieldValue("link",oldData.link)
form.setFieldValue("version",oldData.version)
form.setFieldValue("userpass",oldData.userpass)
// const onFinishimage = () => {
//   let formDaTa = new FormData;
//   formDaTa.append("image",imagedata);
//     Axios.post("http://localhost:8000/api/upload",formDaTa)
//   .then(res => {
//   })
// }
  const onFinish = (values) => {
    let formDaTa = new FormData;
    formDaTa.append("link",values.link);
    formDaTa.append("version",values.version);
    formDaTa.append("userpass",values.userpass);
    formDaTa.append("product_id",values.product_id);
   
    if (values.upload) {
      formDaTa.append("image", values.upload[0].originFileObj);
    }
    addDetails(formDaTa).then((res)=>{
      navigate('/admin/product');
    })


  }
  const props = {
   
    beforeUpload: (file) => {
      setFile(file)
      console.log(file.type)
      const isPNG = file.type === 'image/jpeg/png';
      if (!isPNG) {
        message.success(`${file.name} jpg file`);
      }
      return isPNG || Upload.LIST_IGNORE;
    },
    onChange: (info) => {
    },
  };
  if(oldData.length >= 1 ){
    return (
      <Form {...layout}  form={form}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
      <h2>Override Product Details </h2>
      <Form.Item
          name="link" 
          label="Link"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input  />
        </Form.Item>
        <Form.Item
          name="userpass" 
          label="userpass"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input value="" />
        </Form.Item>
  
        <Form.Item label="Version "
        name="version">
          <Select>
            <Select.Option value="dev">Dev</Select.Option>
            <Select.Option value="prod">Prod</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="product_id"
          label="ProID"
          hidden={true}
          rules={[
              {
                required: true,
              },
            ]}
        >
          <Input disabled={true}  />
        </Form.Item>
        <Form.Item
        name="image"
        label="Image"
        >
          {/* <Input type="file" name="image" onChange={e =>handleChangeimage(e.target.files)} id="image" /> */}
          <Upload {...props} >
            <Button name="icon" id="icon" icon={<UploadOutlined />}>Thay Đổi</Button>
          </Upload>
        </Form.Item>
        <Form.Item
          wrapperCol={{
            ...layout.wrapperCol,
            offset: 8,
          }}
        >
          <Form.Item>
          </Form.Item>
          <Button type="primary"  htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    );
  }else{
    return(
      <Form {...layout}  form={form}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
      <h2>New Product Details </h2>
      <Form.Item
          name="link" 
          label="Link"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input  />
        </Form.Item>
        <Form.Item
          name="userpass" 
          label="userpass"
          rules={[
              {
                required: true,
              },
            ]}
            >
          <Input value="" />
        </Form.Item>
        <Form.Item label="Version "
        name="version">
          <Select>
            <Select.Option value="dev">Dev</Select.Option>
            <Select.Option value="prod">Prod</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          name="product_id"
          label="Pro ID"
          hidden={true}
          rules={[
              {
                required: true,
              },
            ]}
        >
          <Input disabled="true"  />
        </Form.Item>
        <Form.Item
          label="Upload"
          name="upload"
          valuePropName="fileList"
          getValueFromEvent={normFile}
                  >
                    <Upload name="pic" customRequest={dummyRequest} listType="picture">
                      <Button icon={<UploadOutlined />}>Click to upload</Button>
                    </Upload>
                  </Form.Item>
                  <Form.Item>
        <Button type="primary"  htmlType="submit">
            Submit
          </Button>
        </Form.Item>
        
      </Form>
    );
  }
};
export default AddDetails;
