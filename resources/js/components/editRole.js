import { useEffect,useContext,useState } from "react";
import { useNavigate , useParams} from "react-router-dom";
import http from '../http'
import React from 'react';
import { AppContext } from "../store";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { Button, Form, Input,Image ,Select } from 'antd';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
const EditRole = () => {
const [form] = Form.useForm();
const { getRole , getEditUser,putRole } = useContext(AppContext);

const navigate = useNavigate();
// const [oneProduct , setOneProduct] = useState([]);
const {id} = useParams([]);
var someValue = window.localStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var id_user = obj.id;
const [inputs,setInputs] = useState({});
const [dataone,setDataOne] = useState({});

const hinh = "http://localhost/laravel/reactjsnew/hehehe/public/images/"+dataone.icon
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}
const fetchOne = ()=>{
    http.get('/users/'+id+'/edit').then(res=>{
      form.setFieldsValue(res.data)
      setDataOne(res.data);
      setInputs({
        name:res.data.name,
        email:res.data.email,
        password:res.data.password,
        phanquyen:res.data.phanquyen,
      })
    }
    )
}
useEffect(()=>{
    getEditUser(id).then((res)=>{
      console.log(res.data)
      form.setFieldsValue(res.data)
      setDataOne(res.data);
      setInputs({
        name:res.data.name,
        email:res.data.email,
        password:res.data.password,
        phanquyen:res.data.phanquyen,
        role_id:res.data.role_id,
        role_idv2:res.data.role_id,
      })
    })
},[])

const onFinish = (inputs) => {
  inputs.name = id_user;
  inputs.email = id ;
  console.log(inputs)
  putRole(id,inputs).then((res)=>{
    navigate('/admin')
  })
  setInputs(inputs) ;

  }
  return (
    <Form {...layout}  form={form}  dataSource={dataone}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
    <h2>ROLE</h2>
    <ToastContainer />
    <Form.Item label="Vai Trò Tại "
          name="quyen">

            {/* {console.log("asdas",inputs.role_id)} */}
            {

             inputs.role_id != 1  && <> <Input value={"User"} /></>

            }
            {
             inputs.role_id == 1 && <> <Input value={"Admin"} /></>

            }
            

        </Form.Item>

        <Form.Item label=" Quyền Hiện Tại "
          name="quyen2">

            {console.log("asdas",inputs.role_idv2)}
          {
            inputs.role_idv2 == 1 && <> <Input value={" Admin"}/></>
          }
          {
            inputs.role_idv2 == 0 && <> <Input value={" Chưa có quyền"}/></>
          }
            {
             inputs.role_idv2 == 11 &&
               <> <Input value={"Dev"} /></>

            }
            {
             inputs.role_idv2 == 22 && 
              <> <Input value={"Prod"} /></>

            }

        </Form.Item>
        <Form.Item label="Vai Trò"
          name="role">
          <Select
           value={inputs.role || ''} onChange={handleChange} 
          >
            <Select.Option  value="11">DEV</Select.Option>
            <Select.Option value="22">PROD</Select.Option>
          
          </Select>
        </Form.Item>

      
      <Form.Item
        wrapperCol={{
          ...layout.wrapperCol,
          offset: 8,
        }}
      >
        <Button type="primary"  htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form >
  );
};
export default EditRole;
