
import { useEffect, useState ,useContext } from "react";
import { UploadOutlined } from '@ant-design/icons';
import { useNavigate , useParams} from "react-router-dom";
import http from '../http'
import React from 'react';
import { AppContext } from "../store";
import { Button, Form, Input,Image, Upload } from 'antd';
import TextArea from "antd/es/input/TextArea";
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};
const validateMessages = {
  required: '${label} is required!',
  types: {
    email: '${label} is not a valid email!',
    number: '${label} is not a valid number!',
  },
  number: {
    range: '${label} must be between ${min} and ${max}',
  },
};
const Editprod = () => {
  var someValue = window.localStorage.getItem('user');
  var obj = JSON.parse(someValue); 
  var id_user = obj.id;


const { getEditPro , getEditUser ,putEditPro} = useContext(AppContext);

const [form] = Form.useForm();
const [file, setFile] = useState({});

const navigate = useNavigate();
let {id_pro} =useParams();
const [inputs,setInputs] = useState({});
const [dataone,setDataOne] = useState({});


const hinh = "http://localhost/"+dataone.icon
const handleChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    setInputs(values => ({...values,[name]:value}))
}

const props = {
  beforeUpload: (file) => {
    setFile(file)
    const isPNG = file.type === 'image/cc' ;
    if (!isPNG) {
      message.error(`${file.name} is not a jpg file`);
    }
    return isPNG || Upload.LIST_IGNORE;
  },
  onChange: (info) => {
  },
};
const fetchOne = ()=>{
    http.get('/products/'+id_pro+'/edit').then(res=>{
      form.setFieldsValue(res.data)
      console.log("res.data",res.data)
      setDataOne(res.data);
      setInputs({
        id_pro:res.data.id_pro,
        name_pro:res.data.name_pro,
        phanloai:res.data.phanloai,
        mota:res.data.mota,
        icon:res.data.mota
      })
    }
    )
}
useEffect(()=>{
   // fetchOne();
   getEditPro(id_pro).then((res)=>{
    form.setFieldsValue(res.data)
    setDataOne(res.data);
    setInputs({
      id_pro:res.data.id_pro,
      name_pro:res.data.name_pro,
      phanloai:res.data.phanloai,
      mota:res.data.mota,
      icon:res.data.mota
    })
    })

},[])

const onFinish = (inputs) => {
    inputs.icon = file.name
    inputs.id_user = id_user
    // http.put('/products/'+id_pro,inputs).then((res)=>{
    //     navigate('/admin/product');
    // })  

    putEditPro(id_pro,inputs).then((res)=>{
       navigate('/admin/product');
    })
    setInputs(inputs) ;
  }
  return (
    <Form {...layout}  form={form}  dataSource={dataone}  name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
    <h2>EDIT</h2>
      <Form.Item
        name="name_pro" 
        label="Name"  
        rules={[
            {
              required: true,
            },
          ]}
          >
        <Input   value={inputs.name_pro || ''} onChange={handleChange} />
      </Form.Item>
      <Form.Item
        name="phanloai"
        label="Phân Loại"
        rules={[
            {
              required: true,
            },
          ]}
      >
        <Input value={inputs.phanloai || ''} onChange={handleChange}  />
      </Form.Item>
      <Form.Item
        name='mota'
        label="Mô Tả"
      >
       <TextArea value={inputs.mota || ''} onChange={handleChange}  >
     </TextArea>
      </Form.Item>
      <Form.Item
        name="icon"
        label="Icon"
      >
       <Image src={hinh} width={100} height={100} />
      </Form.Item>
      <Form.Item
        label="Thay Icon "
      >
          <Upload {...props} >
            <Button name="icon" id="icon" icon={<UploadOutlined />}>Thay Đổi</Button>
          </Upload>
      </Form.Item>
      <Form.Item
      className="submitalighn"
        wrapperCol={{
          ...layout.wrapperCol,
          offset: 8,
        }}
      >
        <Button type="primary"  htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form >
  );
};
export default Editprod;
